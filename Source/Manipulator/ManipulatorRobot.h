// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ManipulatorRobot.generated.h"

enum class ManipulatorRobotState
{
    SEARCHING,
    MOVING,
    WAITING
};

UCLASS()
class MANIPULATOR_API AManipulatorRobot : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AManipulatorRobot();

public:
    UPROPERTY(EditDefaultsOnly)
    class UPoseableMeshComponent* mesh;

    UPROPERTY(EditDefaultsOnly)
    float waitingDelay;

    UPROPERTY(EditDefaultsOnly)
    float rotationSpeed;

    UPROPERTY(EditDefaultsOnly)
    FName rootBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName swingBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName lowerArmBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName upperArmBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName rotatorBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName bendBoneName;

    UPROPERTY(EditDefaultsOnly)
    FName twistBoneName;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
    static float calculateAngleBetween(float side1, float side2, float frontSide);

    AActor* findFirstActor(TSubclassOf<AActor> actorClass) const;
    void searchForTarget(float deltaTime);
    void setupWaitTimer();

    void calculateTargetRotations(const FVector& destination);
    void rotateJoints(float deltaTime);

    FRotator getBoneRotation(FName boneName) const;
    FVector getBoneLocation(FName boneName) const;

    // Returns the bone rotation in the relation of the parent bone.
    FRotator getBoneLocalRotation(FName boneName, FName parentBoneName) const;
    // Sets the bone rotation in the relation of the parent bone.
    void setBoneLocalRotation(FName boneName, FName parentBoneName, FRotator localRotation);

    FRotator getTwistLocalRotation() const;
    void setTwistLocalRotation(const FRotator& rotation);

    FRotator getBendLocalRotation() const;
    void setBendLocalRotation(const FRotator& rotation);

    FRotator getRotatorLocalRotation() const;
    void setRotatorLocalRotation(const FRotator& rotation);

    FRotator getUpperArmLocalRotation() const;
    void setUpperArmLocalRotation(const FRotator& rotation);

    FRotator getLowerArmLocalRotation() const;
    void setLowerArmLocalRotation(const FRotator& rotation);

    FRotator getSwingLocalRotation() const;
    void setSwingLocalRotation(const FRotator& rotation);

private:
    FTimerHandle currentTimer;
    bool hasMovableObject;
    ManipulatorRobotState currentState;

    FRotator swingRotation;
    FRotator lowerArmRotation;
    FRotator upperArmRotation;
    FRotator rotatorRotation;
    FRotator bendRotation;
    FRotator twistRotation;

    float swingBoneTargetRotation;
    float lowerArmBoneTargetRotation;
    float upperArmBoneTargetRotation;

    float swingBoneInitialRotation;
    float lowerArmBoneInitialRotation;
    float upperArmBoneInitialRotation;

    float deltaTimeCounter;

};
