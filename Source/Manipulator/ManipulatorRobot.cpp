// Fill out your copyright notice in the Description page of Project Settings.

#include "ManipulatorRobot.h"
#include "MovableObject.h"
#include "DestinationObject.h"
#include "Components/PoseableMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AManipulatorRobot::AManipulatorRobot()
{
	PrimaryActorTick.bCanEverTick = true;

    mesh = CreateDefaultSubobject<UPoseableMeshComponent>(TEXT("RobotMesh"));

    rotationSpeed = 0.5f;
    waitingDelay = 3.f;

    rootBoneName = TEXT("BASE_STATIC");
    swingBoneName = TEXT("BASE_ROT");
    lowerArmBoneName = TEXT("ARM_1");
    upperArmBoneName = TEXT("UPBASE");
    rotatorBoneName = TEXT("ARM_2");
    bendBoneName = TEXT("HEAD_ST");
    twistBoneName = TEXT("HEAD_ND");
    currentState = ManipulatorRobotState::SEARCHING;

    hasMovableObject = false;
}

// Called when the game starts or when spawned
void AManipulatorRobot::BeginPlay()
{
	Super::BeginPlay();

    swingRotation = getSwingLocalRotation();
    lowerArmRotation = getLowerArmLocalRotation();
    upperArmRotation = getUpperArmLocalRotation();
    rotatorRotation = getRotatorLocalRotation();
    bendRotation = getBendLocalRotation();
    twistRotation = getTwistLocalRotation();

}

AActor* AManipulatorRobot::findFirstActor(TSubclassOf<AActor> actorClass) const
{
    TArray<AActor*> foundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), actorClass, foundActors);
    if (foundActors.Num() == 0)
    {
        return nullptr;
    }
    return foundActors[0];
}

// Called every frame
void AManipulatorRobot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    switch (currentState) {
        case ManipulatorRobotState::SEARCHING:
            searchForTarget(DeltaTime);
            break;
        case ManipulatorRobotState::MOVING:
            rotateJoints(DeltaTime);
            break;
        default:
            break;
    }

}

void AManipulatorRobot::searchForTarget(float deltaTime)
{
    TSubclassOf<AActor> actorClass = hasMovableObject ?
    ADestinationObject::StaticClass() : AMovableObject::StaticClass();
    AActor* firstActor = findFirstActor(actorClass);
    if (!firstActor)
    {
        return;
    }
    const FVector destination = firstActor->GetActorLocation();
    calculateTargetRotations(destination);
    deltaTimeCounter = deltaTime;
    currentState = ManipulatorRobotState::MOVING;
}

float AManipulatorRobot::calculateAngleBetween(float side1, float side2, float frontSide)
{
    return FMath::RadiansToDegrees(FMath::Acos(
            (side1 * side1 + side2 * side2 - frontSide * frontSide) / (2.f * side1 * side2)));
}

void AManipulatorRobot::setupWaitTimer()
{
    auto handle = [this]() {
        hasMovableObject = !hasMovableObject;
        currentState = ManipulatorRobotState::SEARCHING;
        GetWorldTimerManager().ClearTimer(currentTimer);
    };
    GetWorldTimerManager().SetTimer(currentTimer, handle, waitingDelay, false);
}

void AManipulatorRobot::calculateTargetRotations(const FVector& destination)
{
    const FVector swingLocation = getBoneLocation(swingBoneName);
    const FVector lowerArmLocation = getBoneLocation(lowerArmBoneName);
    const FVector twistLocation = getBoneLocation(twistBoneName);
    const FVector upperArmLocation = getBoneLocation(upperArmBoneName);

    const FVector swingDestinationVector = destination - swingLocation;
    const FVector swingBoneNormalVector = lowerArmLocation - swingLocation;
    const float swingAngleDelta = swingDestinationVector.Rotation().Yaw -
        swingBoneNormalVector.Rotation().Yaw;

    const FRotator swingRotator(0.f, swingAngleDelta, 0.f);

    const FVector rotatedLowerArmLocation = swingRotator.RotateVector(
            lowerArmLocation - swingLocation) + swingLocation;
    const FVector lowerArmTwistVector = swingRotator.RotateVector(twistLocation - lowerArmLocation);
    const FVector lowerArmUpperArmVector = swingRotator.RotateVector(
            upperArmLocation - lowerArmLocation);
    const FVector upperArmTwistVector = swingRotator.RotateVector(twistLocation - upperArmLocation);
    const FVector targetLowerArmVector = destination - rotatedLowerArmLocation;

    const float oldUpperArmAngle = calculateAngleBetween(
            lowerArmUpperArmVector.Size(), upperArmTwistVector.Size(), lowerArmTwistVector.Size());
    const float newUpperArmAngle = calculateAngleBetween(
            lowerArmUpperArmVector.Size(), upperArmTwistVector.Size(), targetLowerArmVector.Size());
    const float upperArmAngleDelta = newUpperArmAngle - oldUpperArmAngle;

    const FRotator lowerArmTwistRotationDelta = FQuat::FindBetweenVectors(
                                        lowerArmTwistVector, targetLowerArmVector).Rotator();

    const float oldLowerArmAngle = calculateAngleBetween(
            lowerArmUpperArmVector.Size(), lowerArmTwistVector.Size(), upperArmTwistVector.Size());
    const float newLowerArmAngle = calculateAngleBetween(
            lowerArmUpperArmVector.Size(), targetLowerArmVector.Size(), upperArmTwistVector.Size());
    const float lowerArmAngleDelta =
            oldLowerArmAngle + lowerArmTwistRotationDelta.Roll - newLowerArmAngle;

    swingBoneTargetRotation = swingRotation.Pitch + swingAngleDelta;
    lowerArmBoneTargetRotation = lowerArmRotation.Roll + lowerArmAngleDelta;
    upperArmBoneTargetRotation = upperArmRotation.Roll - upperArmAngleDelta;

    swingBoneInitialRotation = swingRotation.Pitch;
    lowerArmBoneInitialRotation = lowerArmRotation.Roll;
    upperArmBoneInitialRotation = upperArmRotation.Roll;
}

void AManipulatorRobot::rotateJoints(float deltaTime)
{
    deltaTimeCounter = FMath::Clamp(deltaTimeCounter + deltaTime * 0.5f, 0.f, 1.f);

    swingRotation.Pitch = FMath::Lerp(swingBoneInitialRotation, swingBoneTargetRotation, deltaTimeCounter);
    setSwingLocalRotation(swingRotation);

    lowerArmRotation.Roll = FMath::Lerp(lowerArmBoneInitialRotation, lowerArmBoneTargetRotation, deltaTimeCounter);
    setLowerArmLocalRotation(lowerArmRotation);

    upperArmRotation.Roll = FMath::Lerp(upperArmBoneInitialRotation, upperArmBoneTargetRotation, deltaTimeCounter);
    setUpperArmLocalRotation(upperArmRotation);

    if (deltaTimeCounter == 1.f)
    {
        currentState = ManipulatorRobotState::WAITING;
        setupWaitTimer();
    }
}

FVector AManipulatorRobot::getBoneLocation(FName boneName) const
{
    return mesh->GetBoneLocationByName(boneName, EBoneSpaces::Type::WorldSpace);
}

FRotator AManipulatorRobot::getBoneRotation(FName boneName) const
{
    return mesh->GetBoneRotationByName(boneName, EBoneSpaces::Type::WorldSpace);
}

FRotator AManipulatorRobot::getBoneLocalRotation(FName boneName, FName parentBoneName) const
{
    auto parentWorldRotation = mesh->GetBoneQuaternion(parentBoneName, EBoneSpaces::Type::WorldSpace);
    auto boneWorldRotation = mesh->GetBoneQuaternion(boneName, EBoneSpaces::Type::WorldSpace);
    return (parentWorldRotation.Inverse() * boneWorldRotation).Rotator();
}

void AManipulatorRobot::setBoneLocalRotation(FName boneName, FName parentBoneName, FRotator localRotation)
{
    auto parentWorldRotation = mesh->GetBoneQuaternion(parentBoneName, EBoneSpaces::Type::WorldSpace);
    FRotator boneWorldRotation = (parentWorldRotation * FQuat(localRotation)).Rotator();
    mesh->SetBoneRotationByName(boneName, boneWorldRotation, EBoneSpaces::Type::WorldSpace);
}

FRotator AManipulatorRobot::getTwistLocalRotation() const
{
    return getBoneLocalRotation(twistBoneName, bendBoneName);
}

void AManipulatorRobot::setTwistLocalRotation(const FRotator& rotation)
{
    twistRotation = rotation;
    setBoneLocalRotation(twistBoneName, bendBoneName, rotation);
}

FRotator AManipulatorRobot::getBendLocalRotation() const
{
    return getBoneLocalRotation(bendBoneName, rotatorBoneName);
}

void AManipulatorRobot::setBendLocalRotation(const FRotator& rotation)
{
    bendRotation = rotation;
    setBoneLocalRotation(bendBoneName, rotatorBoneName, rotation);
}

FRotator AManipulatorRobot::getRotatorLocalRotation() const
{
    return getBoneLocalRotation(rotatorBoneName, upperArmBoneName);
}

void AManipulatorRobot::setRotatorLocalRotation(const FRotator& rotation)
{
    rotatorRotation = rotation;
    setBoneLocalRotation(rotatorBoneName, upperArmBoneName, rotation);
}

FRotator AManipulatorRobot::getUpperArmLocalRotation() const
{
    return getBoneLocalRotation(upperArmBoneName, lowerArmBoneName);
}

void AManipulatorRobot::setUpperArmLocalRotation(const FRotator& rotation)
{
    upperArmRotation = rotation;
    setBoneLocalRotation(upperArmBoneName, lowerArmBoneName, rotation);
}

FRotator AManipulatorRobot::getLowerArmLocalRotation() const
{
    return getBoneLocalRotation(lowerArmBoneName, swingBoneName);
}

void AManipulatorRobot::setLowerArmLocalRotation(const FRotator& rotation)
{
    lowerArmRotation = rotation;
    setBoneLocalRotation(lowerArmBoneName, swingBoneName, rotation);
}

FRotator AManipulatorRobot::getSwingLocalRotation() const
{
    return getBoneLocalRotation(swingBoneName, rootBoneName);
}

void AManipulatorRobot::setSwingLocalRotation(const FRotator& rotation)
{
    swingRotation = rotation;
    setBoneLocalRotation(swingBoneName, rootBoneName, rotation);
}
