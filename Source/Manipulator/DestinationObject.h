// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DestinationObject.generated.h"

UCLASS()
class MANIPULATOR_API ADestinationObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestinationObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
